# OpenML dataset: Perth-House-Prices

https://www.openml.org/d/43822

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Acknowledgements
This data was scraped from http://house.speakingsame.com/ and includes data from 322 Perth suburbs, resulting in an average of about 100 rows per suburb.
Content
I believe the columns chosen to represent this dataset are the most crucial in predicting house prices. Some preliminary analysis I conducted showed a significant correlation between each of these columns and the response variable (i.e. price). 
Data obtained from other than scrape source
Longitude and Latitude data was obtained from data.gov.au.
School ranking data was obtained from bettereducation.
The nearest schools to each address selected in this dataset are schools which are defined to be 'ATAR-applicable'. In the Australian secondary school education system, ATAR is a scoring system used to assess a student's culminative academic results and is used for entry into Australian universities. As such, schools which do not have an ATAR program such as primary schools, vocational schools, special needs schools etc. are not considered in determining the nearest school.
Do also note that under the "NEAREST_SCH_RANK" column, there are some missing rows as some schools are unranked according to this criteria by bettereducation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43822) of an [OpenML dataset](https://www.openml.org/d/43822). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43822/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43822/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43822/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

